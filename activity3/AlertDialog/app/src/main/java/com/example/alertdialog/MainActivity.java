package com.example.alertdialog;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText Input = findViewById(R.id.et_input);
        Button show = findViewById(R.id.bt_show);

        Show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                String Text = Input.getText().toString();
                if (Text.isEmpty()){
                    alert ("Please Insert Data !!!");

                }else {
                    alert(Text);
                }
            }
        }
        }
        private void alert (String message){
        AlertDialog dlg = new AlertDialog.Buildee (context: MainActivity.this)
                .setTitle("Message")
                .setMessage(message)
                    setPositiveButton(text: "OK", new DialogInterface.OnClickListiner() {
            @Override
                public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }

            }

        }
    }